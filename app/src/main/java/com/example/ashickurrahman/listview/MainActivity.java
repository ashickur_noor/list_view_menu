package com.example.ashickurrahman.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;
import java.util.*;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<PersonalInfo> arrayAdapter;
    ArrayList<PersonalInfo> arrayListPerson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView=findViewById(R.id.listview);

        arrayListPerson=new ArrayList<>();

        arrayAdapter=new ArrayAdapter<>(this,R.layout.layout2,arrayListPerson);
        listView.setAdapter(arrayAdapter);

        registerForContextMenu(listView);
        addDataPerson();


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.list_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        Toast.makeText(this, (int) info.id, Toast.LENGTH_SHORT).show();
        return true;
    }

    private void addDataPerson() {
        arrayListPerson.add(new PersonalInfo("Noor","1223"));
        arrayListPerson.add(new PersonalInfo("Noor2","12234"));
    }
}
