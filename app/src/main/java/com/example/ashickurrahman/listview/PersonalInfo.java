package com.example.ashickurrahman.listview;

/**
 * Created by ashickurrahman on 13/2/18.
 */

public class PersonalInfo {
    String name;
    String phone;


    @Override
    public String toString() {
        return "PersonalInfo{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }



    public PersonalInfo (String name,String phone)
    {
        this.name=name;
        this.phone=phone;
    }



}
